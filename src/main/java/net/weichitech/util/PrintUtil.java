package net.weichitech.util;

import org.apache.commons.lang3.time.DateFormatUtils;

public class PrintUtil {
    public static void print(String message) {
        System.out.println(DateFormatUtils.format(System.currentTimeMillis(), "HH:mm:ss--") + message);
    }
}
