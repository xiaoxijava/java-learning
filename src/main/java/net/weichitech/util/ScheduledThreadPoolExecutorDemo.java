package net.weichitech.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPoolExecutorDemo {
    public static void main(String[] args) {
        executeFixedPeriodTaskWithEnoughResource();
    }
    
    /**
     * 执行一个延迟的任务
     */
    private static void executeDelayTask() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        PrintUtil.print("start schedule a task.");
        executor.schedule(new Runnable() {
            @Override
            public void run() {
                PrintUtil.print("task is running.");
            }
        }, 5, TimeUnit.SECONDS);
    }
    
    /**
     * 按照固定的延迟执行任务
     */
    private static void executeFixedDelayTask() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        PrintUtil.print("start schedule a task.");
        executor.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                PrintUtil.print("task is running.");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                PrintUtil.print("task is finished.");
            }
        }, 0, 5, TimeUnit.SECONDS);
    }
    
    /**
     * 按照固定的周期执行任务
     */
    private static void executeFixedPeriodTask() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);
        PrintUtil.print("start schedule a task.");
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                PrintUtil.print("task is running.");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                PrintUtil.print("task is finished.");
            }
        }, 0, 5, TimeUnit.SECONDS);
    }
    
    /**
     * 按照固定的周期执行任务多个任务，但是资源够。
     */
    private static void executeFixedPeriodTaskWithEnoughResource() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
        PrintUtil.print("start schedule a task.");
        executor.scheduleAtFixedRate(new DemoTask("task1", 3000), 0, 5, TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(new DemoTask("task2", 3000), 0, 5, TimeUnit.SECONDS);
    }
    
    /**
     * 按照固定的周期执行任务多个任务，但是资源不够了。
     */
    private static void executeFixedPeriodTaskWithNoResource() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        PrintUtil.print("start schedule a task.");
        executor.scheduleAtFixedRate(new DemoTask("task1", 3000), 0, 5, TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(new DemoTask("task2", 3000), 0, 5, TimeUnit.SECONDS);
    }
    
    static class DemoTask implements Runnable {
        private String name;
        private long taskExecuteTime;
        
        DemoTask(String name, long taskExecuteTime) {
            this.name = name;
            this.taskExecuteTime = taskExecuteTime;
        }
        
        @Override
        public void run() {
            PrintUtil.print(name + " is running.");
            try {
                Thread.sleep(taskExecuteTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            PrintUtil.print(name + " is finished.");
        }
    }
}
