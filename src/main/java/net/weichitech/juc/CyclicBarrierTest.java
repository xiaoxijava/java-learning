package net.weichitech.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierTest {
    private static SexStatistics[] sexStatisticsData;// 统计数据存放数组
    private static CyclicBarrier cb;// 循环栅栏
    
    public static void main(String[] args) {
        sexStatisticsData = new SexStatistics[5];
        cb = new CyclicBarrier(5, new Runnable() {
            @Override
            public void run() {
                // 最终汇总男女生数量
                int totalMale = 0, totalFemale = 0;
                for (SexStatistics stat : sexStatisticsData) {
                    totalMale += stat.male;
                    totalFemale += stat.female;
                }
                System.out.println("The school has male:" + totalMale + ",female:" + totalFemale);
            }
        });
        // 这里启动5个线程去执行每个班级的男女统计
        for (int i = 0; i < 5; i++) {
            new Thread(new TeacherWork(i)).start();
        }
    }
    
    static class TeacherWork implements Runnable {
        private int classNo;
        
        public TeacherWork(int classNo) {
            this.classNo = classNo;
        }
        
        @Override
        public void run() {
            SexStatistics stat = new SexStatistics();
            stat.male = (int) (Math.random() * 50); // 这里用随机数模拟统计
            stat.female = (int) (Math.random() * 50); // 这里用随机数模拟统计
            sexStatisticsData[classNo] = stat;
            System.out.println("Class:" + classNo + " has male:" + stat.male + ",female:" + stat.female);
            try {
                cb.await();
            } catch (InterruptedException e) {
                return;
            } catch (BrokenBarrierException e) {
                return;
            }
        }
    }
    
    static class SexStatistics {
        private int male;
        private int female;
    }
    
}
