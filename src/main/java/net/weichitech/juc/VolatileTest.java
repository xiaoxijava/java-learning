package net.weichitech.juc;

public class VolatileTest {
    private volatile static boolean flag;
    
    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (flag) {
                        System.out.println("I am thread1, flag is true");
                        flag = false;
                    }
                }
            }
        };
        t1.start();
        
        Thread t2 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (flag == false) {
                        System.out.println("I am thread2, flag is false");
                        flag = true;
                    }
                }
            }
        };
        t2.start();
        
    }
    
    private static int a;
    private static int b;
    private static volatile int c;
    private static int d;
    private static int e;
    
    public static void testOrder() {
        a = 1;
        b = 2;
        c = 3;
        d = 4;
        e = 5;
    }
    
    public static void testOrder2() {
        e = 5;
        d = 4;
        c = 3;
        b = 2;
        a = 1;
    }
}
