package net.weichitech.juc;

public class VolatileNotWorkTest {
    private static volatile int value = 0;
    
    public static void main(String[] args) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    value++;
                }
            }
        };
        
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        t1.start();
        t2.start();
        
        while(Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println(value);
    }
    
   
}
