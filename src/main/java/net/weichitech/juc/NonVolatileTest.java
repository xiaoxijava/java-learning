package net.weichitech.juc;

public class NonVolatileTest {
    private static boolean flag;
    
    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (flag) {
                        System.out.println("I am thread1, flag is true");
                        flag = false;
                    }
                }
            }
        };
        t1.start();
        
        Thread t2 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (flag == false) {
                        System.out.println("I am thread2, flag is false");
                        flag = true;
                    }
                }
            }
        };
        t2.start();
        
    }
    
}
