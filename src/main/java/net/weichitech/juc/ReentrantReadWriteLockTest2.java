package net.weichitech.juc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockTest2 {
    
    public static void main(String[] args) throws Exception {
        testGetWriteLockAfterUnlockReadLockForSameThread();
    }
    
    /**
     * 当前线程先获取读锁，不能再获取写锁，需要等读锁释放才行
     */
    public static void testGetWriteLockAfterUnlockReadLockForSameThread() throws Exception {
        DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();
        readLock.lock();
        System.out.println(df.format(new Date()) + "MainThread got read lock.");
        Thread.sleep(500);
        System.out.println(df.format(new Date()) + "MainThread try to get write lock.");
        writeLock.tryLock(3, TimeUnit.SECONDS);
        System.out.println(
                df.format(new Date()) + "Current write lock hold count is:" + readWriteLock.getWriteHoldCount());
        readLock.unlock();
    }
    
    /**
     * 同时获取写锁，则排队依次执行
     */
    public static void testWriteLockConcurrently() {
        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        for (int i = 0; i < 3; i++) {
            DemoThread thread = new DemoThread("WriteThread" + i, readWriteLock.writeLock());
            thread.start();
        }
    }
    
    static class DemoThread extends Thread {
        private DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        private Lock lock;
        
        public DemoThread(String name, Lock lock) {
            super(name);
            this.lock = lock;
        }
        
        @Override
        public void run() {
            try {
                lock.lock();
                System.out.println(df.format(new Date()) + getName() + " is working.");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.println(df.format(new Date()) + getName() + " finished work.");
                lock.unlock();
            }
            
        }
    }
}
