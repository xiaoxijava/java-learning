package net.weichitech.juc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ArrayBlockingQueueTest {
    
    public static void main(String[] args) {
        DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        BlockingQueue<Integer> bq = new ArrayBlockingQueue<>(10);
        Thread consumerThread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Integer value = bq.take();
                        System.out.println(df.format(new Date()) + "get " + value + " from queue");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        consumerThread.start();
        
        Thread producerThread = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    try {
                        bq.put(i);
                        System.out.println(df.format(new Date()) + "put " + i + " to queue");
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        producerThread.start();
    }
}
