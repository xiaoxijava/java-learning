package net.weichitech.juc;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerTest {
    
    private static volatile int value;
    
    private static AtomicInteger atomicInteger = new AtomicInteger(0);
    
    public static void main(String[] args) {
        testIncreaseWithAtomicInteger();
    }
    
    private static void testIncreaseWithAtomicInteger() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    atomicInteger.incrementAndGet();
                }
            }
        };
        
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        t1.start();
        t2.start();
        
        while (Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println(atomicInteger.get());
    }
    
    private static void testIncreaseWithSync() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    increaseBySync();
                }
            }
        };
        
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        t1.start();
        t2.start();
        
        while (Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println(value);
    }
    
    private static synchronized int increaseBySync() {
        return value++;
    }
    
}
