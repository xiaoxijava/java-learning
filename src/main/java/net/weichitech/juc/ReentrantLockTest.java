package net.weichitech.juc;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockTest {
    private static volatile int value;
    
    private static Lock lock = new ReentrantLock();
    
    public static void main(String[] args) {
        testIncreaseWithLock();
    }
    
    private static void testIncreaseWithLock() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    lock.lock();
                    for (int i = 0; i < 1000; i++) {
                        value++;
                    }
                } finally {
                    lock.unlock();
                }
            }
        };
        
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        t1.start();
        t2.start();
        
        while (Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println(value);
    }
    
    private static void testIncreaseWithSync() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    increaseBySync();
                }
            }
        };
        
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        t1.start();
        t2.start();
        
        while (Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println(value);
    }
    
    private static synchronized int increaseBySync() {
        return value++;
    }
    
    public static void methodA(){
        try{
            lock.lock();
            //dosomething
            methodB();
        }finally{
            lock.unlock();
        }
    }
    
    public static void methodB(){
        try{
            lock.lock();
            //dosomthing
        }finally{
            lock.unlock();
        }
    }
}
