package net.weichitech.juc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockTest {
    
    public static void main(String[] args) throws Exception {
        testReadLockWhenSameThreadHasWriteLockAndAQSHasReadThreadForUnfairLock();
    }
    
    /**
     * 测试当读写锁是非公平锁时，当前线程获取写锁后，如果AQS有其他线程要获取读锁，可以立刻获取读锁
     * 
     * @throws Exception
     */
    private static void testReadLockWhenSameThreadHasWriteLockAndAQSHasReadThreadForUnfairLock() throws Exception {
        DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock(false);
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        DemoThread3 readWriteThread = new DemoThread3("ReadWriteThread", lock);
        readWriteThread.start();
        Thread.sleep(1000);
        System.out.println(df.format(new Date()) +"MainThread start to gain ReadLock");
        readLock.lock();
        System.out.println(df.format(new Date()) +"MainThread got ReadLock");
        Thread.sleep(5000);
        readLock.unlock();
        System.out.println(df.format(new Date()) +"MainThread released ReadLock");
    }
    
    static class DemoThread3 extends Thread {
        private DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        private ReentrantReadWriteLock lock;
        
        public DemoThread3(String name, ReentrantReadWriteLock lock) {
            super(name);
            this.lock = lock;
        }
        
        @Override
        public void run() {
            try {
                ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
                ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
                writeLock.lock();
                System.out.println(df.format(new Date()) + getName() + " got WriteLock.");
                Thread.sleep(2000);
                System.out.println(df.format(new Date()) + getName() + " try to get ReadLock.");
                readLock.lock();
                System.out.println(df.format(new Date()) + getName() + " got ReadLock.");
                writeLock.unlock();
                System.out.println(df.format(new Date()) + getName() + " release WriteLock.");
                readLock.unlock();
                System.out.println(df.format(new Date()) + getName() + " released ReadLock.");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
    }
    
    /**
     * 测试当读写锁是公平锁时，当前线程获取写锁后，如果AQS有其他线程要获取读锁，也需要排队获取读锁
     * 
     * @throws Exception
     */
    private static void testReadLockWhenSameThreadHasWriteLockAndAQSHasReadThreadForFairLock() throws Exception {
        DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        DemoThread2 readWriteThread = new DemoThread2("ReadWriteThread", lock);
        readWriteThread.start();
        Thread.sleep(1000);
        System.out.println(df.format(new Date()) +"MainThread start to gain ReadLock");
        readLock.lock();
        System.out.println(df.format(new Date()) +"MainThread got ReadLock");
        Thread.sleep(5000);
        readLock.unlock();
        System.out.println(df.format(new Date()) +"MainThread released ReadLock");
    }
    
    /**
     * 测试当前线程获取写锁后，如果AQS有其他线程要获取写锁，需要排队获取读锁
     * 
     * @throws Exception
     */
    private static void testReadLockWhenSameThreadHasWriteLockAndAQSHasWriteThread() throws Exception {
        DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
        DemoThread2 readWriteThread = new DemoThread2("ReadWriteThread", lock);
        readWriteThread.start();
        Thread.sleep(1000);
        System.out.println(df.format(new Date()) +"MainThread start to gain WriteLock");
        writeLock.lock();
        System.out.println(df.format(new Date()) +"MainThread got WriteLock");
        Thread.sleep(5000);
        writeLock.unlock();
        System.out.println(df.format(new Date()) +"MainThread released WriteLock");
    }
    
    static class DemoThread2 extends Thread {
        private DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        private ReentrantReadWriteLock lock;
        
        public DemoThread2(String name, ReentrantReadWriteLock lock) {
            super(name);
            this.lock = lock;
        }
        
        @Override
        public void run() {
            try {
                ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
                ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
                writeLock.lock();
                System.out.println(df.format(new Date()) + getName() + " got WriteLock.");
                Thread.sleep(5000);
                writeLock.unlock();
                System.out.println(df.format(new Date()) + getName() + " release WriteLock.");
                
                System.out.println(df.format(new Date()) + getName() + " try to get ReadLock.");
                readLock.lock();
                System.out.println(df.format(new Date()) + getName() + " got ReadLock.");
                readLock.unlock();
                System.out.println(df.format(new Date()) + getName() + " released ReadLock.");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
    }
    
    /**
     * 测试当前线程获取写锁后，如果AQS没有线程排队也能同时获取读锁
     * 
     * @throws Exception
     */
    private static void testReadLockWhenSameThreadHasWriteLock() throws Exception {
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            System.out.println("I got the write lock.");
            readLock.lock();
            System.out.println("I got the read lock.");
        } finally {
            readLock.unlock();
            writeLock.unlock();
        }
    }
    
    /**
     * 测试当其他线程获取写锁时，当前线程不能同时获取读锁
     * 
     * @throws Exception
     */
    private static void testReadLockWhenAnotherThreadHasWriteLock() throws Exception {
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
        DemoThread readThread = new DemoThread("ReadThread", readLock);
        DemoThread writeThread = new DemoThread("WriteThread", writeLock);
        writeThread.start();
        Thread.sleep(1000);
        readThread.start();
    }
    
    static class DemoThread extends Thread {
        private DateFormat df = new SimpleDateFormat("HH:mm:ss---");
        private Lock lock;
        
        public DemoThread(String name, Lock lock) {
            super(name);
            this.lock = lock;
        }
        
        @Override
        public void run() {
            try {
                lock.lock();
                System.out.println(df.format(new Date()) + getName() + " is working.");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                System.out.println(df.format(new Date()) + getName() + " finished work.");
                lock.unlock();
            }
            
        }
    }
}
