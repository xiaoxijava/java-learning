package net.weichitech.collection;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo {

	public static void main(String[] args) {
		part3();
	}

	/**
	 * HashMap怎么学（二）第一段
	 */
	public static void part2_1() {
		// 步骤一，初始化代码，插入部分12条数据
		Map<Integer, Integer> map = new HashMap<>(20);
		for (int i = 0; i < 12; i++) {
			map.put(i, i);
		}

		// 步骤二，插入第13条数据，触发内部数组长度扩展
		map.put(12, 12);

		// 步骤三，插入特定hash值，触发hash碰撞，生成链表
		for (int i = 1; i < 5; i++) {
			map.put(32 * i, 32 * i);
		}
	}

	/**
	 * HashMap怎么学（一）
	 */
	public static void part1() {
		// 初始化代码，插入12条数据
		Map<Integer, Integer> map = new HashMap<>(20);
		for (int i = 0; i < 12; i++) {
			map.put(i, i);
		}
	}

	/**
	 * HashMap怎么学（三）
	 */
	public static void part3() {
		// 步骤一， 初始化代码，插入64条数据
		Map<Integer, Integer> map = new HashMap<>(128);
		for (int i = 0; i < 64; i++) {
			map.put(i, i);
		}

		// 步骤二，插入特定hash值，触发hash碰撞，生成红黑树
		int i = 1;
		for (; i <= 8; i++) {
			map.put(128 * i, 128 * i);
		}
		
		System.out.println(map);
	}

}
