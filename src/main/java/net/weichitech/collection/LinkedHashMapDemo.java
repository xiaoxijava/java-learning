package net.weichitech.collection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHashMapDemo {
    
    public static void main(String[] args) {
        // LinkedHashMap保证插入的有序性
        Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(3, 3);
        map.put(2, 2);
        map.put(4, 4);
        map.get(3);//模拟访问
        System.out.println("LinkedHashMap插入有序性:" + map.keySet());
        
        // LinkedHashMap保证访问的有序性
        map = new LinkedHashMap<>(16, 0.75f, true);
        map.put(1, 1);
        map.put(3, 3);
        map.put(2, 2);
        map.put(4, 4);
        map.get(3);//模拟访问
        System.out.println("LinkedHashMap访问有序性:" + map.keySet());
        
        // HashMap内部实现基于其内部数组
        map = new HashMap<>();
        map.put(1, 1);
        map.put(3, 3);
        map.put(2, 2);
        map.put(4, 4);
        System.out.println("HashMap:" + map.keySet());
    }
}
