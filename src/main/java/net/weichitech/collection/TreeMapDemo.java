package net.weichitech.collection;

import java.util.TreeMap;

public class TreeMapDemo {
	public static void main(String[] args) {
		// 代码段一，初始化treeMap，组成内部红黑树
		TreeMap<Integer, Integer> treeMap = new TreeMap<>();
		for (int i = 0; i < 10; i++) {
			treeMap.put(i, i);
		}

		// 代码段二，keyset是正向有序的
		for(Integer key:treeMap.keySet()) {
			System.out.print(key);
		}
		System.out.println();
		
		// 代码段三，keyset是反向有序的
		for(Integer key:treeMap.descendingKeySet()) {
			System.out.print(key);
		}
	}
}
